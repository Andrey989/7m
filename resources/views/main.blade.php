<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="title" content="Без цен">
  <meta name="description" content="Без цен">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <title>main</title>
  <link rel="stylesheet" href="{{ asset('css/app.2d79ede6efbf9e9d24be.css') }}">


</head>

<body>
  <header>
    <div class="header__home fontello">&#xe814;</div>
    <div class="header__find fontello">&#xe800;</div>
    <div class="header__main">
      <div class="header__main-icon"><span></span></div>
    </div>
    <div class="header__add fontello">&#xf0c9;</div>
    <div class="header__cabinet"><a href="#popup-menu" class="popup-link fontello">&#xe815;</a></div>
  </header>

    

  <div class="main">
    <div class="container">
      <div class="content">
        @yield('content')
      </div>
    </div>
  </div>

  <footer></footer>

  <div id="popup-menu" class="popup">
    <div class="popup__body">
      <div class="popup__content">
        <div class="popup__navigation">
          <span class="popup__close fontello">&#xe817;</span>
          <span>Профиль</span>
          <span class="popup__close"> &#10005; </span>
        </div>
        @if(Auth::check())

        @else
        <div class="popup-menu__login">
          <div>
            Войдите для просмотра <br />
            истории покупок
          </div>
          <span class="popup-menu__login-btn"><a href="#popup-login" class="popup-link">Войти</a></span>
        </div>
        @endif
        <div>
          @if(Auth::check())
          <div class="popup-menu__main-item">
            <span>
              <span class="popup-menu__main-item-icon fontello">&#xe815;</span>
              <a href="#popup-data" class="popup-link">Мои данные</a>
            </span>
            <span class="fontello">&#xf006;</span>
          </div>
          <div class="popup-menu__main-item">
            <span>
              <span class="popup-menu__main-item-icon fontello">&#xe815;</span>
              <a href="#popup-history" class="popup-link">История покупок</a>
            </span>
            <span class="fontello">&#xf006;</span>
          </div>
          <div class="popup-menu__main-item">
            <span>
              <span class="popup-menu__main-item-icon fontello">&#xe815;</span>
              <a href="#popup-discounts" class="popup-link">Персональные скидки</a>
            </span>
            <span class="fontello">&#xf006;</span>
          </div>

          <div class="popup-menu__main-item">
            <span>
              <span class="popup-menu__main-item-icon fontello">&#xe815;</span>
              <a href="#popup-manager" class="popup-link">Управление</a>
            </span>
            <span class="fontello">&#xf006;</span>
          </div>

          @endif
          <div class="popup-menu__main-item">
            <span>
              <span class="popup-menu__main-item-icon fontello">&#xe815;</span>
              <a href="#popup-document" class="popup-link">Обработка перс. данных</a>

            </span>
            <span class="fontello">&#xf006;</span>
          </div>
          <div class="popup-menu__main-item">
            <span> <span class="popup-menu__main-item-icon fontello">&#xe815;</span> Поддержка </span><span
              class="fontello">&#xf006;</span>
          </div>
          <div class="popup-menu__main-item">
            <span> <span class="popup-menu__main-item-icon fontello">&#xe815;</span> Уведомления
            </span><span class="fontello">&#xf006;</span>
          </div>
          <div class="popup-menu__main-item">
            <span> <span class="popup-menu__main-item-icon fontello">&#xe815;</span> Оценка приложения
            </span><span class="fontello">&#xf006;</span>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="popup-login" class="popup">
    <div class="popup__body">
      <div class="popup__content">
        <div class="popup__navigation">
          <a href="#popup-menu" class="popup-link fontello">&#xe817;</a>
          <span>Вход/регистрация</span>
          <span class="popup__close"> &#10005; </span>
        </div>
        <form class="popup-login__form popup__main">
          <div class="popup-login__enter-error error display-none">Неправильный телефон или пароль!</div>
          <input type="text" placeholder="Введите ваш телефон" class="inp-default phone-mask" id="popup-login__tel" />
          <input type="password" placeholder="Введите ваш пароль" class="inp-default" id="popup-login__pass" />
          <div class="popup__buttons">
            <a href="#popup-registration" class="btn-default btn-default_b popup-link">Регистрация</a>
            <input type="submit" value="Войти" class="btn-default" disabled />
          </div>
          <div class="loading display-none"><img src="./assets/images/loading.svg" alt="loading" /></div>
          <a href="">Забыли пароль ?</a>
        </form>
      </div>
    </div>
  </div>

  <div id="popup-registration" class="popup">
    <div class="popup__body">
      <div class="popup__content">
        <div class="popup__navigation">
          <a href="#popup-login" class="popup-link fontello">&#xe817;</a>
          <span>Регистрация</span>
          <span class="popup__close"> &#10005; </span>
        </div>
        <div class="popup__main">
          <form class="popup-registration__step-1">
            <div class="popup-registration__tel-error error display-none">Данный телефон уже
              зарегистрирован!</div>
            <input type="text" placeholder="Введите ваш телефон" class="inp-default phone-mask"
              id="popup-registration__tel" />
            <input type="submit" value="Далее" class="btn-default" disabled />
            <div class="loading display-none"><img src="./assets/images/loading.svg" alt="loading" /></div>
          </form>
          <form class="popup-registration__step-2 display-none">
            <div class="popup-registration__sms-pass-error error display-none">Вы ввели неправильный код!
            </div>
            <input type="text" placeholder="Введите код из СМС" class="inp-default"
              id="popup-registration__sms-pass-inp" />
            <input type="submit" value="Далее" class="btn-default" disabled />
          </form>
          <form class="popup-registration__step-3 display-none">
            <input type="text" placeholder="Имя*" class="inp-default" id="popup-registration__name" />
            <input type="text" placeholder="Фамилия*" class="inp-default" id="popup-registration__surname" />
            <input type="text" placeholder="Отчество" class="inp-default" id="popup-registration__patronymic" />
            <input type="text" placeholder="Дата рождения*" class="inp-default" id="popup-registration__birth"
              onfocus="(this.type='date')" />
            <input type="email" placeholder="Email*" class="inp-default" id="popup-registration__email" />
            <div class="popup-registration__gender-box">
              Пол*:&nbsp;&nbsp;&nbsp;
              <input type="radio" name="popup-registration__gender" id="popup-registration__gender-male" checked />
              <label for="popup-registration__gender-male">Мужской</label>&nbsp;&nbsp;&nbsp;
              <input type="radio" name="popup-registration__gender" id="popup-registration__gender-female" />
              <label for="popup-registration__gender-female">Женский</label>
            </div>
            <input type="submit" value="Далее" class="btn-default" disabled />
          </form>
          <form class="popup-registration__step-4 display-none">
            <div class="popup-registration__matching-passes-error error display-none">Пароли не совпадают!
            </div>
            <input type="password" placeholder="Пароль, НЕ МЕНЕЕ 8 символов" class="inp-default"
              id="popup-registration__pass-1" />
            <input type="password" placeholder="Повторите введеный пароль" class="inp-default"
              id="popup-registration__pass-2" />
            <input type="submit" value="Завершить" class="btn-default" disabled />
            <div class="loading display-none"><img src="./assets/images/loading.svg" alt="loading" /></div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div id="popup-data" class="popup">
    <div class="popup__body">
      <div class="popup__content">

        @if(Auth::check())

        <div class="popup__navigation">
          <!-- <span class="popup__back fontello">&#xe817;</span> -->
          <a href="#popup-menu" class="popup-link fontello">&#xe817;</a>
          <span>Мои данные</span>
          <span class="popup__close"> &#10005; </span>
        </div>
        <div>
          <div class="popup-data__item">
            <span> Имя: </span>
            <a href="#popup-change-profile" class="popup-link"><?= $user->lastname ?>
              <?= $user->firstname ?>
              <?= $user->patronym ?></a>
          </div>
          <div class="popup-data__item">
            <span> Дата рождения: </span>
            <a href="#popup-change-birth" class="popup-link"><?= $user->born ?></a>
          </div>
          <div class="popup-data__item">
            <span> Телефон: </span>
            <a href="#popup-change-tel" class="popup-data__tel popup-link"><?= $user->phone ?></a>
          </div>
          <div class="popup-data__item">
            <span> Email: </span>
            <a href="#popup-change-email" class="popup-link"><?= $user->email ?></a>
          </div>
          <div class="popup-data__item">
            <span> Пароль: </span>
            <a href="#popup-change-password" class="popup-link">изменить</a>
          </div>
          <!-- <div class="popup-data__item">
              <span> Адрес: </span>
              <a href="">Симферополь, ул. Гагарина 14а</a>
            </div> -->
          {{-- <div class="popup-data__item">
            <span> Номер карты: </span>
            <a href="" class="popup-link">123456789012</a>
          </div> --}}
          <div class="popup-data__item popup-data__item_tumbler">
            <span> Получать СМС: </span>
            <label class="switch">
              <input type="checkbox" checked />
              <span class="switch__slider switch__slider_round"></span>
            </label>
          </div>
          <div class="popup-data__item popup-data__item_tumbler">
            <span> Push-уведомления в лк: </span>
            <label class="switch">
              <input type="checkbox" checked />
              <span class="switch__slider switch__slider_round"></span>
            </label>
          </div>
          <div class="popup-data__item popup-data__item_tumbler">
            <span> Push в программе лояльности: </span>
            <label class="switch">
              <input type="checkbox" checked />
              <span class="switch__slider switch__slider_round"></span>
            </label>
          </div>
          <div class="popup-data__item popup-data__exit">
            <a href="/logout"><strong>Выход</strong></a>
          </div>
        </div>

        @endif
      </div>
    </div>
  </div>
  @if(Auth::check())


  <div id="popup-change-profile" class="popup popup-change">
    <div class="popup__body">
      <div class="popup__content">
        <div class="popup__navigation">
          <a href="#popup-data" class="popup-link fontello">&#xe817;</a>
          <span>Изменение профиля</span>
          <span class="popup__close"> &#10005; </span>
        </div>

        <form class="popup-change-profile__form popup__main">
          <input type="text" value="<?= $user->firstname ?>" placeholder="Имя*" class="inp-default"
            id="popup-change-profile__name" />
          <input type="text" value="<?= $user->lastname ?>" placeholder="Фамилия*" class="inp-default"
            id="popup-change-profile__surname" />
          <input type="text" value="<?= $user->patronym ?>" placeholder="Отчество" class="inp-default"
            id="popup-change-profile__patronymic" />
          <input type="submit" value="Изменить" class="btn-default" disabled />
          <div class="loading display-none"><img src="./assets/images/loading.svg" alt="loading" /></div>
        </form>
      </div>
    </div>
  </div>

  <div id="popup-change-birth" class="popup popup-change">
    <div class="popup__body">
      <div class="popup__content">
        <div class="popup__navigation">
          <a href="#popup-data" class="popup-link fontello">&#xe817;</a>
          <span>Дата рождения</span>
          <span class="popup__close"> &#10005; </span>
        </div>
        <form class="popup-change-birth__form popup__main">
          <input type="date" value="<?= $user->born ?>" placeholder="Дата рождения*" class="inp-default"
            id="popup-change-birth__birth" />
          <input type="submit" value="Изменить" class="btn-default" disabled />
          <div class="loading display-none"><img src="./assets/images/loading.svg" alt="loading" /></div>
        </form>
      </div>
    </div>
  </div>

  <div id="popup-change-tel" class="popup popup-change">
    <div class="popup__body">
      <div class="popup__content">
        <div class="popup__navigation">
          <a href="#popup-data" class="popup-link fontello">&#xe817;</a>
          <span>Телефон</span>
          <span class="popup__close"> &#10005; </span>
        </div>
        <form class="popup-change-tel__step-1 popup__main">
          <div class="popup-change-tel__tel-exist-error error display-none">Данный телефон уже
            зарегистрирован!</div>
          <input type="text" placeholder="Телефон" class="inp-default phone-mask" id="popup-change-tel__tel"
            value="<?= $user->phone ?>" />
          <input type="submit" value="Изменить" class="btn-default" disabled />
          <div class="loading display-none"><img src="./assets/images/loading.svg" alt="loading" /></div>
        </form>
        <form class="popup-change-tel__step-2 popup__main display-none">
          <div class="popup-change-tel__sms-pass-error error display-none">Вы ввели неправильный код!</div>
          <input type="text" placeholder="Введите код из СМС" class="inp-default" id="popup-change-tel__sms-pass-inp" />
          <input type="submit" value="Завершить" class="btn-default" disabled />
          <div class="loading display-none"><img src="./assets/images/loading.svg" alt="loading" /></div>
        </form>
        <div class="popup-change-tel__step-3 popup__main display-none">
          <div class="popup-change-tel__success-icon"><span class="fontello">&#xe810;</span></div>
          <div>Поздравляем, телефон был успешно изменен!</div>
          <button class="popup-change-tel__continue-btn btn-default">Далее</button>
        </div>
      </div>
    </div>
  </div>

  <div id="popup-change-email" class="popup popup-change">
    <div class="popup__body">
      <div class="popup__content">
        <div class="popup__navigation">
          <a href="#popup-data" class="popup-link fontello">&#xe817;</a>
          <span>Электронная почта</span>
          <span class="popup__close"> &#10005; </span>
        </div>
        <form class="popup-change-email__form popup__main">
          <div class="popup-change-email__email-exist-error error display-none">Данный email уже занят!</div>
          <input type="email" value="<?= $user->email ?>" placeholder="Email*" class="inp-default"
            id="popup-change-email__email" />
          <input type="submit" value="Изменить" class="btn-default" disabled />
          <div class="loading display-none"><img src="./assets/images/loading.svg" alt="loading" /></div>
        </form>
      </div>
    </div>
  </div>

  <div id="popup-change-password" class="popup popup-change">
    <div class="popup__body">
      <div class="popup__content">
        <div class="popup__navigation">
          <a href="#popup-data" class="popup-link fontello">&#xe817;</a>
          <span>Изменение пароля</span>
          <span class="popup__close"> &#10005; </span>
        </div>
        <form class="popup-change-password__form popup__main">
          <div class="popup-change-password__matching-passes-error error display-none">Пароли не совпадают!
          </div>
          <input type="password" placeholder="Пароль, НЕ МЕНЕЕ 4 символов" class="inp-default"
            id="popup-change-password__pass-1" />
          <input type="password" placeholder="Повторите пароль" class="inp-default"
            id="popup-change-password__pass-2" />
          <input type="submit" value="Изменить" class="btn-default" disabled />
          <div class="loading display-none"><img src="./assets/images/loading.svg" alt="loading" /></div>
        </form>
      </div>
    </div>
  </div>

  @endif

  <div id="popup-history" class="popup">
    <div class="popup__body">
      <div class="popup__content">
        <div class="popup__navigation">
          <a href="#popup-data" class="popup-link fontello">&#xe817;</a>
          <span>История покупок</span>
          <span class="popup__close"> &#10005; </span>
        </div>
        <div class="popup-history__header">
          <div class="popup-history__points-score"><span class="score-icon">$</span> 0.00 баллов</div>
          <div class="popup-history__header-menu">
            <div>ПОКУПКИ</div>
            <div>МОИ БАЛЛЫ</div>
          </div>
        </div>
        <div class="popup__main">
          <div class="swiper-container popup-history__swiper">
            <div class="swiper-wrapper">
              @if(!empty($storypurchs) && Auth::check())
              @foreach($storypurchs as $item)
              <div class="swiper-slide">
                <h3 class="popup-history__month"><?= $item->created_at ?></h3>
                <div class="popup-history__saved">Сэкономлено с безцен:<span><?= $item->savings ?> &#8381;</span></div>
                <div class="popup-history__points">
                  Начислено баллов:
                  <span class="popup-history__points-added">+<?= $item->bonus ?> <span
                      class="score-icon">$</span></span>
                </div>
                <div class="popup-history__purchases">Сумма покупок: <strong><?= $item->amount ?> &#8381;</strong></div>
                <div class="popup-history__slide-item">
                  <strong>25 июля</strong>
                  <div class="popup-history__slide-item-elem">
                    <div class="fontello">&#xe80a; Время покупки: <strong>22:51</strong></div>
                    <div class="fontello">$ Начислено баллов: <strong>1.5</strong></div>
                    <div class="fontello">&#xe835; Сумма покупки: <strong><?= $item->amount ?> &#8381;</strong></div>
                    <button class="btn-default TEST_API" data-order-id="<?= $item->orderid ?>">Подробнее</button>
                  </div>
                  <div class="popup-history__slide-item-elem">
                    <div class="fontello">&#xe80a; Время покупки: <strong>22:51</strong></div>
                    <div class="fontello">$ Начислено баллов: <strong>1.5</strong></div>
                    <div class="fontello">&#xe835; Сумма покупки: <strong>2055 &#8381;</strong></div>
                    <button class="btn-default">Подробнее</button>
                  </div>
                </div>
                <div class="popup-history__slide-item">
                  <strong>24 июля</strong>
                  <div class="popup-history__slide-item-elem">
                    <div class="fontello">&#xe80a; Время покупки: <strong>21:23</strong></div>
                    <div class="fontello">$ Начислено баллов: <strong>1.5</strong></div>
                    <div class="fontello">&#xe835; Сумма покупки: <strong>2055 &#8381;</strong></div>
                    <button class="btn-default">Подробнее</button>
                  </div>
                </div>
              </div>
              @endforeach
              @endif
            </div>
            <span
              class="popup-history__swiper-nav popup-history__swiper-prev fontello">&#xf007;&nbsp;&nbsp;&nbsp;пред</span>
            <span
              class="popup-history__swiper-nav popup-history__swiper-next fontello">след&nbsp;&nbsp;&nbsp;&#xf006;</span>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="popup-discounts" class="popup">
    <div class="popup__body">
      <div class="popup__content">
        <div class="popup__navigation">
          <a href="#popup-menu" class="popup-link fontello">&#xe817;</a>
          <span>Персональные скидки</span>
          <span class="popup__close"> &#10005; </span>
        </div>
        <div class="popup__main">

          @if (!empty($discounts))
          @foreach ($discounts as $item)

          <div class="personal-promo-slide__inner">
            <figure class="personal-promo-slide__pic" style="">
              <div class="personal-promo-slide__pic-inner">
                <div class="square">
                  <div class="square__inner">
                    <img draggable="false" src="<?= $item->picture ?>" alt="Виноград">
                  </div>
                </div>
              </div>
              <div class="personal-promo-slide__discount discount-tag">
                <span class="discount-tag__inner">
                  -<?= $item->discount ?><sup>%</sup>
                </span>
              </div>
            </figure>
            <div class="personal-promo-slide__meta">
              <div class="personal-promo-slide__meta-inner">
                <h3 class="personal-promo-slide__title">
                  <?= $item->title ?>
                </h3>
                <p class="personal-promo-slide__description">
                  <?= $item->content ?>
                </p>
                <p class="personal-promo-slide__valid-whithin">
                  <?= $item->date ?>
                </p>
              </div>
            </div>
            <div class="promo__activation promo__activation--personal-promo-slide">
              <div class="pp-activation-button__content">
                <a class="button button--small button--primary promo__button" modifiers="small,primary" type="button"
                  href="<?= $item->link_post ?>">
                  <span class="button__inner"><?= $item->btn_slug ?></span>
                </a>
              </div>
            </div>
          </div>

          @endforeach

          @endif


          <style>
          .personal-promo-slide__inner {
            overflow: hidden;
            display: flex;
            flex-flow: column;
            flex: 1;
            -ms-flex-positive: 1;
            -ms-flex-negative: 0;
            -ms-flex-preferred-size: auto;
            height: 100%;
            border-radius: 8px;
            background: #fff;
          }

          .personal-promo-slide__pic {
            position: relative;
            margin: 0;
            /* background: #c4c4c4 url(https://lenta.gcdn.co/contentassets/e5d405f285bf4e3dabcfff2813e2b913/10011_20200212094551.png?preset=medium) no-repeat; */
            background-size: cover;
          }

          .promo__bar-club-pic,
          .promo__kids-club-pic,
          .promo__pic {
            position: relative;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 400px;
            margin: 0 0 20px;
            box-sizing: border-box;
            background: #c4c4c4 url(https://lenta.gcdn.co/static/pics/wood-cropped.6e57281001b17ba3d12328715478f0c0.jpg) no-repeat;
            background-size: cover;
            border-radius: 8px;
          }

          .personal-promo-slide__pic-inner {
            display: flex;
            align-items: center;
            justify-content: center;
            padding: 20px;
            height: 200px;
          }

          .personal-promo-slide__discount {
            position: absolute;
            right: 25px;
            bottom: 27px;
          }

          .discount-tag__inner {
            position: relative;
            font-family: Roboto Slab, Cambria, "Bitstream Vera Serif", Georgia, serif;
            font-size: 36px;
            font-weight: 700;
            line-height: 1;
            z-index: 1;
            display: block;
            min-width: 92px;
            padding: 7px 7px 2px;
            text-align: center;
            box-sizing: border-box;
            border-radius: 5px;
            color: #555;
            background: #fd0;
          }

          .personal-promo-slide__discount {
            position: absolute;
            right: 25px;
            bottom: 27px;
          }

          .square {
            width: 100%;
            position: relative;
          }

          .square__inner {
            position: absolute;
            top: 0;
            left: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            width: 100%;
            height: 100%;
          }

          .square__inner img {
            display: block;
            width: 100%;
          }

          .personal-promo-slide__meta-inner {
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            flex-grow: 1;
          }

          .button.button--small {
            font-size: 15px;
            height: 38px;
            text-transform: none;
            letter-spacing: 0;
          }
          </style>
        </div>
      </div>
    </div>
  </div>

  <div id="popup-manager" class="popup">
    <div class="popup__body">
      <div class="popup__content">
        <div class="popup__navigation">
          <a href="#popup-menu" class="popup-link fontello">&#xe817;</a>
          <span>Управление</span>
          <span class="popup__close"> &#10005; </span>
        </div>
        <div class="popup__main">
          Добавить персональную скидку
          <form method="POST" action="/publish-discounts" enctype="multipart/form-data">
            @csrf

            <input name="picture" type="file" placeholder="Картинка" required>
            <input name="discount" type="number" placeholder="скидка" required>
            <input name="title" type="text" placeholder="Название" required>
            <input name="content" type="text" placeholder="Содержание" required>
            <input name="link_post" type="text" placeholder="Ссылка на пост в инстаграм" required>
            <input name="date" type="text" placeholder="Дата начала и окончания акции" required>
            <input name="btn_slug" type="text" placeholder="Название кнопки" required>

            <button>Сохранить</button>

          </form>



        </div>
      </div>
    </div>
  </div>

  <div id="popup-document" class="popup">
    <div class="popup__body">
      <div class="popup__content">
        <div class="popup__navigation">
          <a href="#popup-menu" class="popup-link fontello">&#xe817;</a>
          <span>Документы</span>
          <span class="popup__close"> &#10005; </span>
        </div>
        <div class="popup__main custom">
          <p>
            Обработка персональных данных Обработка персональных данных Обработка персональных данных
            Обработка персональных данных Обработка
            персональных данных Обработка персональных данных Обработка персональных данных Обработка
            персональных данных Обработка персональных данных
          </p>


          <iframe src="https://jivo.chat/Io19zHFKRy" width="468" height="60" align="left">
            Ваш браузер не поддерживает плавающие фреймы!
          </iframe>

        </div>
      </div>
    </div>
  </div>
  <script src="{{ asset('js/app.2d79ede6efbf9e9d24be.js') }}"></script>
  <script src="{{ asset('js/api.js') }}"></script>
</body>

</html>