@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>





                <div class="card-body">

                    



                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="login-wrap">
                            <div class="login-html">
                                <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Авторизация</label>
                                <input id="tab-2" type="radio" name="tab" class="sign-up"><a href="{{ route('register') }}" for="tab-2" class="tab">Регистрация</a>
                                <div class="login-form">
                                    <div class="sign-in-htm">
                                    <div class="group">
                                            <label for="phone" class="label">Номер телефона</label>
                                            <input id="phone" type="text" class="input form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus>
                                            @error('phone')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="group">
                                            <label for="pass" class="label">Пароль</label>
                                            <input id="password" type="password" class="input form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror


                                        </div>
                                        <div class="group">
                                            <input class="form-check-input check" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <label for="check"><span class="icon"></span> Keep me Signed in</label>
                                        </div>
                                        <div class="group">
                                            <input type="submit" class="button" value="Войти">
                                        </div>
                                        <div class="hr"></div>
                                        <div class="foot-lnk">
                                            @if (Route::has('password.request'))
                                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                                    {{ __('Забыли пароль?') }}
                                                </a>
                                            @endif

                               
                                        </div>
                                    </div>
                                    <div class="sign-up-htm">
                                        <div class="group">
                                            <label for="user" class="label">Username</label>
                                            <input id="user" type="text" class="input">
                                        </div>
                                        <div class="group">
                                            <label for="pass" class="label">Password</label>
                                            <input id="pass" type="password" class="input" data-type="password">
                                        </div>
                                        <div class="group">
                                            <label for="pass" class="label">Repeat Password</label>
                                            <input id="pass" type="password" class="input" data-type="password">
                                        </div>
                                        <div class="group">
                                            <label for="pass" class="label">Email Address</label>
                                            <input id="pass" type="text" class="input">
                                        </div>
                                        <div class="group">
                                            <input type="submit" class="button" value="Sign Up">
                                        </div>
                                        <div class="hr"></div>
                                        <div class="foot-lnk">
                                            <label for="tab-1">Already Member?</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>



            </div>
        </div>
    </div>


    


</div>
@endsection
