@extends('main')

@section('content')
<!-- <div class="container">
    <div class="row justify-content-center">
       
           
           
           
           
           
           
           
        
    </div>
</div> -->
<main class="cabinet">
    <section class="cabinet-personal-data">
        <h2>Личные данные</h2>
        
        <form class="cabinet-personal-data__form">
            <input type="text" value="<?= $user->firstname; ?>" placeholder="Имя*" class="inp-default"
                id="cabinet-personal-data__name" />
            <input type="text" value="<?= $user->lastname; ?>" placeholder="Фамилия*" class="inp-default"
                id="cabinet-personal-data__surname" />
            <input type="text" value="<?= $user->patronym; ?>" placeholder="Отчество" class="inp-default"
                id="cabinet-personal-data__patronymic" />
            <input type="date" value="<?= $user->born; ?>" placeholder="Дата рождения*" class="inp-default"
                id="cabinet-personal-data__birth" />
            <input type="email" value="<?= $user->email; ?>" placeholder="Email*" class="inp-default"
                id="cabinet-personal-data__email" />
            <div>
                Пол*:&nbsp;&nbsp;
                <input type="radio" @if($user->gender == 'male') checked @endif name="cabinet-personal-data__gender"
                id="cabinet-personal-data__gender-male" />
                <label for="cabinet-personal-data__gender-male">Муж</label>&nbsp;&nbsp;
                <input type="radio" @if($user->gender == 'female') checked @endif name="cabinet-personal-data__gender"
                id="cabinet-personal-data__gender-female" />
                <label for="cabinet-personal-data__gender-female">Жен</label>
            </div>
            <input type="submit" value="Изменить личные данные" class="btn-default" />
        </form>
    </section>
    <section class="cabinet-password">
        <h2>Пароль</h2>
        <form class="cabinet-password__form">
            <input type="password" placeholder="Введите новый пароль, длинной не менее 8-ми символов"
                class="inp-default" id="cabinet-password__pass-1" />
            <input type="password" placeholder="Повторите пароль" class="inp-default" id="cabinet-password__pass-2" />
            <input type="submit" value="Изменить пароль" class="btn-default" />
        </form>
    </section>
    <section class="cabinet-contacts">
        <h2>Контакты</h2>
        <div>
            <span class="cabinet-contacts__phone-old"><?= $user->phone; ?></span>
            <a href="#popup-change-tel" class="popup-link btn-default">Изменить телефон</a>
        </div>
    </section>
    <section class="cabinet-purchases">
    
        <h2>История покупок</h2>
        <div class="swiper-container cabinet-purchases__swiper">
            <div class="swiper-wrapper">
            @foreach($storypurchs as $item)
                <div class="swiper-slide">
                    <h3><?= $item->created_at ?></h3>
                    

                    
                    
                    
                    

                    
                    <div>Начислено баллов: <span><?= $item->bonus ?> &#8381;</span></div>
                    <div>Сумма покупок: <span><?= $item->amount ?></span> &#8381;</div>
                    <div>Вы сэкономили с безцен: <span><?= $item->savings ?></span> &#8381;</div>
                    <div class="cabinet-purchases__slide-item">
                        <div><strong> <?= $item->created_at ?> (#<?= $item->id ?>, оплачен)</strong></div>
                        <div>Сумма покупки: <span>2055</span> &#8381;</div>
                        <button class="btn-default">Подробнее</button>
                    </div>
                    <div class="cabinet-purchases__slide-item">
                        <div><strong> 25 июля (15:33, #333, оплачен)</strong></div>
                        <div>Сумма покупки: <span>2055</span> &#8381;</div>
                        <button class="btn-default">Подробнее</button>
                    </div>
                    <div class="cabinet-purchases__slide-item">
                        <div><strong> 25 июля (15:33, #333, оплачен)</strong></div>
                        <div>Сумма покупки: <span>2055</span> &#8381;</div>
                        <button class="btn-default">Подробнее</button>
                    </div>
                    <div class="cabinet-purchases__slide-item">
                        <div><strong> 25 июля (15:33, #333, оплачен)</strong></div>
                        <div>Сумма покупки: <span>2055</span> &#8381;</div>
                        <button class="btn-default">Подробнее</button>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        
    </section>
</main>
@endsection