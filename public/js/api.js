//---------------------------------------------------------
// __functions

const sendHttpRequest = async (method, endpoint, data) => {
  const url = `http://mbezcen.ru/${endpoint}`;
  // const url = `http://testhostru.s55.yourdomain.com.ua/${endpoint}`;
  const csrfToken = document.head.querySelector('[name~=csrf-token][content]').content;
  // const csrfToken = "asas";
  // const url = "https://reqres.in/api/users?delay=2";

  const response = await fetch(url, {
    method,
    body: JSON.stringify(data),
    headers: data ? { 'Content-Type': 'application/json', 'X-CSRF-Token': csrfToken } : {},
  });
  if (!response.ok) {
    const responseError = await response.json();
    const error = new Error('fetch/sendHttpRequest failed');
    error.data = responseError;
    throw error;
  }

  return response.json();
};

//---------------------------------------------------------
// validate email
// https://stackoverflow.com/questions/46155/how-to-validate-an-email-address-in-javascript

function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

//---------------------------------------------------------
// validate date
// https://stackoverflow.com/questions/6177975/how-to-validate-date-with-format-mm-dd-yyyy-in-javascript

function isValidDate(dateString) {
  // First check for the pattern
  var regex_date = /^\d{4}\-\d{1,2}\-\d{1,2}$/;

  if (!regex_date.test(dateString)) {
    return false;
  }

  // Parse the date parts to integers
  var parts = dateString.split('-');
  var day = parseInt(parts[2], 10);
  var month = parseInt(parts[1], 10);
  var year = parseInt(parts[0], 10);

  // Check the ranges of month and year
  if (year < 1000 || year > 3000 || month == 0 || month > 12) {
    return false;
  }

  var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

  // Adjust for leap years
  if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
    monthLength[1] = 29;
  }

  // Check the range of the day
  return day > 0 && day <= monthLength[month - 1];
}

//---------------------------------------------------------
// api

const checkValid = (inp, minLength, maxLength, equalLength) => {
  const len = inp.value.length;
  const minLengthBool = minLength ? len < minLength : false;
  const maxLengthBool = maxLength ? len > maxLength : false;
  const equalLengthBool = equalLength ? len !== equalLength : false;
  if (len === 0 || minLengthBool || maxLengthBool || equalLengthBool) {
    // if (!inp.classList.contains("inp-default__error")) {
    //   inp.classList.add("inp-default__error");
    // }
    return false;
  } else {
    // if (inp.classList.contains("inp-default__error")) {
    //   inp.classList.remove("inp-default__error");
    // }
    return true;
  }
};

const setClass = (el, className, action) => {
  switch (action) {
    case 'ADD':
      if (!el.classList.contains(className)) {
        el.classList.add(className);
      }
      break;
    case 'REMOVE':
      if (el.classList.contains(className)) {
        el.classList.remove(className);
      }
      break;
  }
};

const api = () => {
  const sendLoginData = () => {
    const form = document.getElementsByClassName('popup-login__form')[0];
    const telInp = document.getElementById('popup-login__tel');
    const passInp = document.getElementById('popup-login__pass');
    const error = document.getElementsByClassName('popup-login__enter-error')[0];
    const buttons = form ? form.getElementsByClassName('popup__buttons')[0] : null;
    const loading = form ? form.getElementsByClassName('loading')[0] : null;
    const submitBtn = form ? form.querySelector("input[type='submit']") : null;

    if (!form || !telInp || !error || !buttons || !loading || !submitBtn) return;

    let isTelValid;
    let isPassValid;

    const setDisabled = () => {
      if (isTelValid && isPassValid) {
        submitBtn.disabled = false;
      } else {
        submitBtn.disabled = true;
      }
    };

    telInp.addEventListener('input', () => {
      isTelValid = checkValid(telInp, 18); // работает маска, см _libs -> phome-mask.js
      setDisabled();
    });

    passInp.addEventListener('input', () => {
      isPassValid = checkValid(passInp, 4);
      setDisabled();
    });

    form.addEventListener('submit', async (e) => {
      e.preventDefault();
      setClass(loading, 'display-none', 'REMOVE');
      setClass(buttons, 'display-none', 'ADD');
      try {
        const response = await sendHttpRequest('POST', `login`, {
          phone: telInp.value,
          password: passInp.value,
        });
        if (response.success) {
          // window.location.href = 'http://mbezcen.ru/home';
          location.reload();
        } else {
          setClass(error, 'display-none', 'REMOVE');
        }
        setClass(loading, 'display-none', 'ADD');
        setClass(buttons, 'display-none', 'REMOVE');
        console.log(response);
      } catch (err) {
        console.error('Error response from the server - ', err);
      }
    });
  };
  sendLoginData();

  const sendRegistrationData = () => {
    const form1 = document.getElementsByClassName('popup-registration__step-1')[0];
    const form2 = document.getElementsByClassName('popup-registration__step-2')[0];
    const form3 = document.getElementsByClassName('popup-registration__step-3')[0];
    const form4 = document.getElementsByClassName('popup-registration__step-4')[0];

    if (!form1 || !form2 || !form3 || !form4) return;

    let tel;
    let passSMS;
    let name;
    let surname;
    let patronymic;
    let birth;
    let email;
    let gender;

    //--------------------------------------
    // form-1
    const telInp = document.getElementById('popup-registration__tel');
    const errorForm1 = document.getElementsByClassName('popup-registration__tel-error')[0];
    const submitBtnForm1 = form1.querySelector("input[type='submit']");
    const loadingForm1 = form1.getElementsByClassName('loading')[0];
    if (!telInp || !errorForm1 || !submitBtnForm1 || !loadingForm1) return;

    telInp.addEventListener('input', () => {
      if (checkValid(telInp, 18)) {
        submitBtnForm1.disabled = false;
      } else {
        submitBtnForm1.disabled = true;
      }
    });

    form1.addEventListener('submit', async (e) => {
      e.preventDefault();
      setClass(submitBtnForm1, 'display-none', 'ADD');
      setClass(loadingForm1, 'display-none', 'REMOVE');

      try {
        const response = await sendHttpRequest('POST', `registeruser`, {
          tel: telInp.value,
        });
        tel = telInp.value;
        console.log(response); // response.success true - телефон не зареган, false - телефон не зареган
        if (response.pass) {
          passSMS = response.pass;
          setClass(form1, 'display-none', 'ADD');
          setClass(form2, 'display-none', 'REMOVE');
          setClass(errorForm1, 'display-none', 'ADD');
        } else {
          setClass(errorForm1, 'display-none', 'REMOVE');
        }

        setClass(submitBtnForm1, 'display-none', 'REMOVE');
        setClass(loadingForm1, 'display-none', 'ADD');
      } catch (err) {
        console.error('Error response from the server - ', err);
      }
    });

    //--------------------------------------
    // form-2
    const passSMSInp = document.getElementById('popup-registration__sms-pass-inp');
    const errorForm2 = document.getElementsByClassName('popup-registration__sms-pass-error')[0];
    const submitBtnForm2 = form2.querySelector("input[type='submit']");
    if (!passSMSInp || !errorForm2 || !submitBtnForm2) return;

    passSMSInp.addEventListener('input', () => {
      if (checkValid(passSMSInp, 4)) {
        submitBtnForm2.disabled = false;
      } else {
        submitBtnForm2.disabled = true;
      }
    });

    form2.addEventListener('submit', (e) => {
      e.preventDefault();
      if (passSMSInp.value === String(passSMS)) {
        setClass(form2, 'display-none', 'ADD');
        setClass(form3, 'display-none', 'REMOVE');
        setClass(errorForm2, 'display-none', 'ADD');
        passSMSInp.value = '';
      } else {
        setClass(errorForm2, 'display-none', 'REMOVE');
      }
    });

    //--------------------------------------
    // form-3
    const nameInp = document.getElementById('popup-registration__name');
    const surnameInp = document.getElementById('popup-registration__surname');
    const patronymicInp = document.getElementById('popup-registration__patronymic');
    const birthInp = document.getElementById('popup-registration__birth');
    const emailInp = document.getElementById('popup-registration__email');
    const genderMaleRadio = document.getElementById('popup-registration__gender-male');
    const submitBtnForm3 = form3.querySelector("input[type='submit']");

    if (!nameInp || !surnameInp || !patronymicInp || !birthInp || !genderMaleRadio || !submitBtnForm3) return;

    let isNameValid;
    let isSurnameValid;
    let isBirthValid;
    let isEmailValid;

    const setForm3SubDisabled = () => {
      if (isNameValid && isSurnameValid && isBirthValid && isEmailValid) {
        submitBtnForm3.disabled = false;
      } else {
        submitBtnForm3.disabled = true;
      }
    };

    nameInp.addEventListener('input', () => {
      isNameValid = checkValid(nameInp, 1);
      setForm3SubDisabled();
    });

    surnameInp.addEventListener('input', () => {
      isSurnameValid = checkValid(surnameInp, 1);
      setForm3SubDisabled();
    });

    birthInp.addEventListener('input', () => {
      isBirthValid = isValidDate(birthInp.value);
      setForm3SubDisabled();
    });

    emailInp.addEventListener('input', () => {
      isEmailValid = validateEmail(emailInp.value);
      setForm3SubDisabled();
    });

    form3.addEventListener('submit', async (e) => {
      e.preventDefault();
      name = nameInp.value;
      surname = surnameInp.value;
      patronymic = patronymicInp.value;
      birth = birthInp.value;
      email = emailInp.value;
      gender = genderMaleRadio.checked ? 'male' : 'female';
      setClass(form3, 'display-none', 'ADD');
      setClass(form4, 'display-none', 'REMOVE');
    });

    //--------------------------------------
    // form-4
    const pass1Inp = document.getElementById('popup-registration__pass-1');
    const pass2Inp = document.getElementById('popup-registration__pass-2');
    const errorForm4 = document.getElementsByClassName('popup-registration__matching-passes-error')[0];
    const submitBtnForm4 = form4.querySelector("input[type='submit']");
    const loadingForm4 = form4.getElementsByClassName('loading')[0];

    if (!pass1Inp || !pass2Inp || !submitBtnForm4 || !loadingForm4) return;

    let isPass1Valid;
    let isPass2Valid;

    const setForm4SubDisabled = () => {
      if (isPass1Valid && isPass2Valid) {
        submitBtnForm4.disabled = false;
      } else {
        submitBtnForm4.disabled = true;
      }
    };

    pass1Inp.addEventListener('input', () => {
      isPass1Valid = checkValid(pass1Inp, 4);
      setForm4SubDisabled();
    });

    pass2Inp.addEventListener('input', () => {
      isPass2Valid = checkValid(pass2Inp, 4);
      setForm4SubDisabled();
    });

    form4.addEventListener('submit', async (e) => {
      e.preventDefault();
      setClass(loadingForm4, 'display-none', 'REMOVE');
      setClass(submitBtnForm4, 'display-none', 'ADD');

      if (pass1Inp.value === pass2Inp.value) {
        setClass(errorForm4, 'display-none', 'ADD');
        try {
          const response = await sendHttpRequest('POST', `register`, {
            phone: tel,
            firstname: name,
            lastname: surname,
            patronym: patronymic,
            born: birth,
            email,
            gender,
            password: pass1Inp.value,
          });
          if (response.success) location.reload();
          console.log(response);
          setClass(loadingForm4, 'display-none', 'ADD');
          setClass(submitBtnForm4, 'display-none', 'REMOVE');
        } catch (err) {
          console.error('Error response from the server - ', err);
        }
      } else {
        setClass(errorForm4, 'display-none', 'REMOVE');
      }
    });
  };
  sendRegistrationData();

  const changeProfileData = () => {
    const form = document.getElementsByClassName('popup-change-profile__form')[0];
    const nameInp = document.getElementById('popup-change-profile__name');
    const surnameInp = document.getElementById('popup-change-profile__surname');
    const patronymicInp = document.getElementById('popup-change-profile__patronymic');
    const submitBtn = form ? form.querySelector("input[type='submit']") : null;
    const loading = form ? form.getElementsByClassName('loading')[0] : null;

    if (!form || !nameInp || !surnameInp || !patronymicInp || !submitBtn || !loading) return;

    let oldName = nameInp.value;
    let oldSurname = surnameInp.value;
    let oldPatronymic = patronymicInp.value;

    let isNameValid = checkValid(nameInp, 1);
    let isSurnameValid = checkValid(surnameInp, 1);

    const setDisabled = () => {
      const inpsValid = isNameValid && isSurnameValid;
      const nameChange = oldName !== nameInp.value;
      const surnameChange = oldSurname !== surnameInp.value;
      const patronymicChange = oldPatronymic !== patronymicInp.value;
      if (inpsValid && (nameChange || surnameChange || patronymicChange)) {
        submitBtn.disabled = false;
      } else {
        submitBtn.disabled = true;
      }
    };

    nameInp.addEventListener('input', () => {
      isNameValid = checkValid(nameInp, 1);
      setDisabled();
    });

    surnameInp.addEventListener('input', () => {
      isSurnameValid = checkValid(surnameInp, 1);
      setDisabled();
    });

    patronymicInp.addEventListener('input', () => {
      setDisabled();
    });

    form.addEventListener('submit', async (e) => {
      e.preventDefault();
      setClass(loading, 'display-none', 'REMOVE');
      setClass(submitBtn, 'display-none', 'ADD');

      try {
        const response = await sendHttpRequest('POST', `userupdate`, {
          firstname: nameInp.value,
          lastname: surnameInp.value,
          patronym: patronymicInp.value,
        });
        if (response.success) {
          oldName = nameInp.value;
          oldSurname = surnameInp.value;
          oldPatronymic = patronymicInp.value;
          setDisabled();
        }
        console.log(response);
        setClass(loading, 'display-none', 'ADD');
        setClass(submitBtn, 'display-none', 'REMOVE');
      } catch (err) {
        console.error('Error response from the server - ', err);
      }
    });
  };
  changeProfileData();

  const changeBirthDate = () => {
    const form = document.getElementsByClassName('popup-change-birth__form')[0];
    const birthInp = document.getElementById('popup-change-birth__birth');
    const submitBtn = form ? form.querySelector("input[type='submit']") : null;
    const loading = form ? form.getElementsByClassName('loading')[0] : null;

    if (!form || !birthInp || !submitBtn || !loading) return;

    let oldBirthDate = birthInp.value;

    birthInp.addEventListener('input', () => {
      const isBirthDateValid = isValidDate(birthInp.value);
      const isDatesNotEqual = oldBirthDate !== birthInp.value;
      if (isBirthDateValid && isDatesNotEqual) {
        submitBtn.disabled = false;
      } else {
        submitBtn.disabled = true;
      }
    });

    form.addEventListener('submit', async (e) => {
      e.preventDefault();
      setClass(submitBtn, 'display-none', 'ADD');
      setClass(loading, 'display-none', 'REMOVE');

      const newBirthDate = birthInp.value;

      try {
        const response = await sendHttpRequest('POST', `updatebornuser`, {
          born: newBirthDate,
        });
        if (response.success) {
          oldBirthDate = newBirthDate;
          submitBtn.disabled = true;
        }
        console.log(response);
        setClass(submitBtn, 'display-none', 'REMOVE');
        setClass(loading, 'display-none', 'ADD');
      } catch (err) {
        console.error('Error response from the server - ', err);
      }
    });
  };
  changeBirthDate();

  const changeTel = () => {
    const form1 = document.getElementsByClassName('popup-change-tel__step-1')[0];
    const form2 = document.getElementsByClassName('popup-change-tel__step-2')[0];
    const step3 = document.getElementsByClassName('popup-change-tel__step-3')[0];

    if (!form1 || !form2 || !step3) return;

    //--------------------------------------
    // step-1
    const telInp = document.getElementById('popup-change-tel__tel');
    const errorForm1 = document.getElementsByClassName('popup-change-tel__tel-exist-error')[0];
    const subBtnForm1 = form1.querySelector("input[type='submit']");
    const loadingForm1 = form1.getElementsByClassName('loading')[0];

    if (!telInp || !errorForm1 || !subBtnForm1 || !loadingForm1) return;

    let oldTel = telInp.value;
    let newTel;
    let passFromSms;

    telInp.addEventListener('input', () => {
      if (checkValid(telInp, 18) && oldTel !== telInp.value.substring(0, 18)) {
        subBtnForm1.disabled = false;
      } else {
        subBtnForm1.disabled = true;
      }
    });

    form1.addEventListener('submit', async (e) => {
      e.preventDefault();
      setClass(subBtnForm1, 'display-none', 'ADD');
      setClass(loadingForm1, 'display-none', 'REMOVE');
      newTel = telInp.value;
      try {
        const response = await sendHttpRequest('POST', `updatecontacts`, {
          phoneOld: oldTel,
          phoneNew: newTel,
        });
        if (response.success) {
          passFromSms = response.pass;
          setClass(form1, 'display-none', 'ADD');
          setClass(form2, 'display-none', 'REMOVE');
          setClass(errorForm1, 'display-none', 'ADD');
        } else {
          setClass(errorForm1, 'display-none', 'REMOVE');
        }
        console.log(response);
        setClass(subBtnForm1, 'display-none', 'REMOVE');
        setClass(loadingForm1, 'display-none', 'ADD');
      } catch (err) {
        console.error('Error response from the server - ', err);
      }
    });

    //--------------------------------------
    // step-2
    const errorForm2 = document.getElementsByClassName('popup-change-tel__sms-pass-error')[0];
    const smsPassInp = document.getElementById('popup-change-tel__sms-pass-inp');
    const subBtnForm2 = form2.querySelector("input[type='submit']");
    const loadingForm2 = form2.getElementsByClassName('loading')[0];

    smsPassInp.addEventListener('input', () => {
      const isSMSPassValid = checkValid(smsPassInp, 4);
      console.log(isSMSPassValid);
      if (isSMSPassValid) {
        subBtnForm2.disabled = false;
      } else {
        subBtnForm2.disabled = true;
      }
    });

    form2.addEventListener('submit', async (e) => {
      e.preventDefault();
      if (smsPassInp.value === String(passFromSms)) {
        setClass(errorForm2, 'display-none', 'ADD');
        setClass(subBtnForm2, 'display-none', 'ADD');
        setClass(loadingForm2, 'display-none', 'REMOVE');
        try {
          const response = await sendHttpRequest('POST', `updatesmscontacts`, {
            phoneOld: oldTel,
            phoneNew: newTel,
          });
          console.log(response);
          if (response.success) {
            setClass(form2, 'display-none', 'ADD');
            setClass(step3, 'display-none', 'REMOVE');
            setClass(subBtnForm2, 'display-none', 'REMOVE');
            setClass(loadingForm2, 'display-none', 'ADD');
            oldTel = telInp.value;
            subBtnForm1.disabled = true;
            subBtnForm2.disabled = true;
            smsPassInp.value = '';
          }
        } catch (err) {
          console.error('Error response from the server - ', err);
        }
      } else {
        setClass(errorForm2, 'display-none', 'REMOVE');
      }
    });

    //--------------------------------------
    // step-3
    const continueBtn = document.getElementsByClassName('popup-change-tel__continue-btn')[0];

    continueBtn.addEventListener('click', () => {
      console.log(oldTel);
      setClass(form1, 'display-none', 'REMOVE');
      setClass(step3, 'display-none', 'ADD');
    });
  };
  changeTel();

  const changeEmail = () => {
    const form = document.getElementsByClassName('popup-change-email__form')[0];
    const emailInp = document.getElementById('popup-change-email__email');
    const error = document.getElementsByClassName('popup-change-email__email-exist-error')[0];
    const submitBtn = form ? form.querySelector("input[type='submit']") : null;
    const loading = form ? form.getElementsByClassName('loading')[0] : null;

    if (!form || !emailInp || !error || !submitBtn || !loading) return;

    let oldEmail = emailInp.value;

    emailInp.addEventListener('input', () => {
      const isEmailValid = validateEmail(emailInp.value);
      const isEmailsNotEqual = oldEmail !== emailInp.value;
      if (isEmailValid && isEmailsNotEqual) {
        submitBtn.disabled = false;
      } else {
        submitBtn.disabled = true;
      }
    });

    form.addEventListener('submit', async (e) => {
      e.preventDefault();
      setClass(submitBtn, 'display-none', 'ADD');
      setClass(loading, 'display-none', 'REMOVE');

      try {
        const newEmail = emailInp.value;
        const response = await sendHttpRequest('POST', `updatemailuser`, {
          email: newEmail,
        });
        if (response.success) {
          oldEmail = newEmail;
          submitBtn.disabled = true;
          setClass(error, 'display-none', 'ADD');
        } else {
          setClass(error, 'display-none', 'REMOVE');
        }
        console.log(response);
        setClass(submitBtn, 'display-none', 'REMOVE');
        setClass(loading, 'display-none', 'ADD');
      } catch (err) {
        console.error('Error response from the server - ', err);
      }
    });
  };
  changeEmail();

  const changePass = () => {
    const form = document.getElementsByClassName('popup-change-password__form')[0];
    const tel = document.getElementsByClassName('popup-data__tel')[0];
    const pass1Inp = document.getElementById('popup-change-password__pass-1');
    const pass2Inp = document.getElementById('popup-change-password__pass-2');
    const error = document.getElementsByClassName('popup-change-password__matching-passes-error')[0];
    const submitBtn = form ? form.querySelector("input[type='submit']") : null;
    const loading = form ? form.getElementsByClassName('loading')[0] : null;

    if (!form || !tel || !pass1Inp || !pass2Inp || !error || !submitBtn || !loading) return;

    let isPass1Valid;
    let isPass2Valid;

    const setDisabled = () => {
      if (isPass1Valid && isPass2Valid) {
        submitBtn.disabled = false;
      } else {
        submitBtn.disabled = true;
      }
    };

    pass1Inp.addEventListener('input', () => {
      isPass1Valid = checkValid(pass1Inp, 4); // работает маска, см _libs -> phome-mask.js
      setDisabled();
    });

    pass2Inp.addEventListener('input', () => {
      isPass2Valid = checkValid(pass2Inp, 4); // работает маска, см _libs -> phome-mask.js
      setDisabled();
    });

    form.addEventListener('submit', async (e) => {
      e.preventDefault();
      if (pass1Inp.value === pass2Inp.value) {
        setClass(submitBtn, 'display-none', 'ADD');
        setClass(loading, 'display-none', 'REMOVE');
        setClass(error, 'display-none', 'ADD');
        try {
          const response = await sendHttpRequest('POST', `updatepass`, {
            phone: tel.textContent,
            pass: pass1Inp.value,
          });
          console.log(response);
          if (response.success) {
            setClass(submitBtn, 'display-none', 'REMOVE');
            setClass(loading, 'display-none', 'ADD');
          }
        } catch (err) {
          console.error('Error response from the server - ', err);
        }
      } else {
        setClass(error, 'display-none', 'REMOVE');
      }
    });
  };
  changePass();

  const getOrderDetails = () => {
    const btn = document.getElementsByClassName('TEST_API')[0];

    btn.addEventListener('click', async () => {
      const response = await sendHttpRequest('GET', `orderdetails?id=${btn.getAttribute('data-order-id')}`);
      console.log(response);
    });
  };
  getOrderDetails();
};
api();
