<?php

namespace App\Http\Controllers;


use Auth;
use App\Storypurchase;
use App\Discount;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index(Request $request){

        $user = Auth::user();
        $storypurchs = Storypurchase::All();
        $discounts = Discount::All();
        return view('main', compact('user', 'storypurchs', 'discounts'));


    }



}
