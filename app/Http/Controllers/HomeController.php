<?php

namespace App\Http\Controllers;

use Auth;
use App\Storypurchase;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $storypurchs = Storypurchase::All();
        return view('home', compact('user', 'storypurchs'));
    }
}
