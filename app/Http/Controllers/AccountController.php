<?php

namespace App\Http\Controllers;

use Auth;
use App\Storypurchase;
use App\User;
use App\Storyorder;
use App\Discount;

use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function index(){

    

    }

    public function userupdate(Request $request){


       $triger = array();
       $triger = array(
           "success" => false
       );

       if (Auth::check()) {
           $user = Auth::user();
           $user->firstname = $request['firstname'];
           $user->lastname = $request['lastname'];
           $user->patronym = $request['patronym'];
           $user->save();

           $triger = array(
               "success" => true
           );

       }

        return json_encode($triger, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);


    }

    public function updatepass(Request $request){

       $resetpas = array();

          $userrestore = User::where('phone', $request['phone'])->first();

          if(!empty($userrestore)){

               $userrestore->password = bcrypt($request['pass']);

               $userrestore->save();


               $resetpas = array(
                   'success' => true
               );

          }else{
           $resetpas = array(
               'success' => false
           );
          }

          


       return json_encode($resetpas, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);



    }



    public function updatecontacts(Request $request){

        
       $resetpas = array();

       $userrestore = User::where('phone', $request['phoneOld'])->first();

       if(!empty($userrestore)){

        $msg = rand(1940, 95022);

        $resetpas = array(
            'success' => true,
            "pass" => $msg,
            'phonenew' => $request['phoneNew']
        );

       }else{
        $resetpas = array(
            'success' => false
        );
       }

    return json_encode($resetpas, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

    }


    public function updatesmscontacts(Request $request){

        
        $resetpas = array();
 
        $userrestore = User::where('phone', $request['phoneOld'])->first();
 
        if(!empty($userrestore)){

            $userrestore->phone = $request['phoneNew'];
            $userrestore->save();
 
         $resetpas = array(
             'success' => true
         );
 
        }else{
         $resetpas = array(
             'success' => false
         );
        }
 
     return json_encode($resetpas, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
 
     }



    public function updatemailuser(Request $request){


        $triger = array();
        $triger = array(
            "success" => false
        );
 
        if (Auth::check()) {
            $user = Auth::user();
            $user->email = $request['email'];
            $user->save();
 
            $triger = array(
                "success" => true
            );
 
        }
 
         return json_encode($triger, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
 
 
     }

     public function updatebornuser(Request $request){


        $triger = array();
        $triger = array(
            "success" => false
        );
 
        if (Auth::check()) {
            $user = Auth::user();
            $user->born = $request['born'];
            $user->save();
 
            $triger = array(
                "success" => true
            );
 
        }
 
         return json_encode($triger, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
 
 
     }



     
    public function orderdetails(Request $request){

        $arrorder = array();

        $order = Storyorder::where('orderid', $request['id'])->get();

        $arrorder = array(
            'order' => $order,
            'success' => true
        );


        return json_encode($arrorder, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
 
     }




     public function publishDiscounts(Request $request){

        $path_photo = 'uploads/default.png';
        if ($request->file('picture')) {
            $imagePath = $request->file('picture');
            $imageName = $imagePath->getClientOriginalName();
            $path_photo = $request->file('picture')->storeAs('uploads', $imageName, 'public');
          }

          $discount = new Discount;
          $discount->picture = '/storage/'.$path_photo;
          $discount->title = $request['title'];
          $discount->content = $request['content'];
          $discount->discount = $request['discount'];
          $discount->date = $request['date'];
          $discount->btn_slug = $request['btn_slug'];
          $discount->link_post = $request['link_post'];
          
          $discount->save();

          return redirect('/');

     }




}
