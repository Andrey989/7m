<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'MainController@index')->name('index');

Route::post('/registeruser', 'LoggedController@registeruser')->name('registeruser');

Route::post('/userupdate', 'AccountController@userupdate')->name('userupdate');

Route::post('/updatepass', 'AccountController@updatepass')->name('updatepass');

Route::post('/updatecontacts', 'AccountController@updatecontacts')->name('updatecontacts');

Route::post('/updatecontacts', 'AccountController@updatecontacts')->name('updatecontacts');

Route::post('/updatemailuser', 'AccountController@updatemailuser')->name('updatemailuser');
Route::post('/updatebornuser', 'AccountController@updatebornuser')->name('updatebornuser');


Route::post('/publish-discounts', 'AccountController@publishDiscounts')->name('publishDiscounts');





Route::post('/updatesmscontacts', 'AccountController@updatesmscontacts')->name('updatesmscontacts');

Route::get('/logout', 'LoggedController@logout')->name('logout');

Route::get('/orderdetails', 'AccountController@orderdetails')->name('orderdetails');




